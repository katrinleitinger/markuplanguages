import {ShoppingList} from "./shoppingList";

var sl = new ShoppingList();

sl.addArticle("Milch", 1.2, 2);
sl.addArticle("Brot", 2, 1);
sl.addArticle("Butter", 0.8, 3);

sl.printResult();
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const item_1 = require("./item");
class ShoppingList {
    constructor() {
        this.list = new Array();
    }
    addArticle(article, price, amount) {
        var i = new item_1.Item();
        i.article = article;
        i.price = price;
        i.amount = amount;
        i.total = price * amount;
        this.list.push(i);
    }
    printResult() {
        let sum = 0;
        for (let j = 0; j < this.list.length; j++) {
            console.log("Artikel: " + this.list[j].article);
            console.log("Preis: " + this.list[j].price);
            console.log("Menge: " + this.list[j].amount);
            console.log("€: " + this.list[j].total.toFixed(2));
            sum = sum + this.list[j].total;
        }
        console.log(" ");
        console.log("GesamtPreis: " + sum.toFixed(2));
    }
}
exports.ShoppingList = ShoppingList;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const shoppingList_1 = require("./shoppingList");
var sl = new shoppingList_1.ShoppingList();
sl.addArticle("Milch", 1.2, 2);
sl.addArticle("Brot", 2, 1);
sl.addArticle("Butter", 0.8, 3);
sl.printResult();

var ShoppingList = /** @class */ (function () {
    function ShoppingList() {
        this.list = new Array();
    }
    ShoppingList.prototype.addArticle = function (article, price, amount) {
        var i = new Item();
        i.article = article;
        i.price = price;
        i.amount = amount;
        i.total = price * amount;
        this.list.push(i);
    };
    ShoppingList.prototype.printResult = function () {
        var sum = 0;
        for (var j = 0; j < this.list.length; j++) {
            console.log("Artikel: " + this.list[j].article);
            console.log("Preis: " + this.list[j].price);
            console.log("Menge: " + this.list[j].amount);
            console.log("€: " + this.list[j].total);
            sum = sum + this.list[j].total;
        }
        console.log(" ");
        console.log("GesamtPreis: " + sum);
    };
    return ShoppingList;
}());
var Item = /** @class */ (function () {
    function Item() {
    }
    return Item;
}());

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  public message: string = "Hello Angular";
  public showDiv: boolean = true;

  public persons:Array<Person> = [
    new Person("Susi", 12),
    new Person("Hugo", 30),
    new Person("Max", 20)
  ]

  constructor() { }

  ngOnInit() {
  }

  public updateMessage():void{
    this.message += "!"
  }

}

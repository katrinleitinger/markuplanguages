<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.1">
	<xsl:template match="/todo">
		<html>
			<head>
				<title><xsl:value-of select="@name"/></title>
			</head>
			<body>
				<h1><xsl:value-of select="@name"/></h1>
				<xsl:apply-templates select="item"/>
			</body>
		</html>		
	</xsl:template>
	<xsl:template match="item">
		<p>			
			<xsl:attribute name="id"><xsl:value-of select="@titel"/></xsl:attribute>
			<xsl:comment><xsl:value-of select="@status"/></xsl:comment>
			<div>
				<strong><xsl:value-of select="@titel"/></strong>
			</div>
			<div>
				<xsl:value-of select="."/>
			</div>
			<div>
				Status: <xsl:value-of select="@status"/>
			</div>
		</p>
	</xsl:template>
</xsl:stylesheet>